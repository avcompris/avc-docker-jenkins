# avc-docker-jenkins

Docker image: avcompris/jenkins

Usage:

	$ docker run -d \
		-v /data/jenkins:/var/jenkins_home \
		avcompris/jenkins

Or full configuration:

	$ docker run -d --name jenkins \
	    -v /data/jenkins:/var/jenkins_home \
	    -v /etc/jenkins/m2:/etc/jenkins/m2:ro \
	    -e JENKINS_USER=dandriana \
	    -e JENKINS_PASSWORD=603a...aa \
	    -e MUNIN_NODE_NAME=sample-jenkins.avcompris.com \
	    -e MUNIN_NODE_ALLOW=^172\\.17\\.0\\..+$ \
	    avcompris/jenkins

Exposed port is 8080.

See additional documentation
[here](https://github.com/jenkinsci/docker/blob/master/README.md)