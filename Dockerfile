# File: avc-docker-jenkins/Dockerfile
#
# Use to build the image: avcompris/jenkins

FROM jenkins/jenkins:2.228-jdk11
MAINTAINER david.andriana@avantage-compris.com

USER root

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

# libjson-perl is used by Munin plugins: jenkins_*
#
RUN apt-get install -y \
	ntp \
	munin-node \
	libjson-perl \
	maven=3.3.9-4

#-------------------------------------------------------------------------------
#   2. MUNIN PLUGINS
#-------------------------------------------------------------------------------

COPY munin/plugins/jenkins_       /usr/share/munin/plugins/
COPY munin/plugins/jenkins_nodes_ /usr/share/munin/plugins/

RUN ln -s /usr/share/munin/plugins/jenkins_ /etc/munin/plugins/jenkins_results
RUN ln -s /usr/share/munin/plugins/jenkins_ /etc/munin/plugins/jenkins_queue
RUN ln -s /usr/share/munin/plugins/jenkins_ /etc/munin/plugins/jenkins_running

COPY munin/plugin-conf.d/jenkins_ /etc/munin/plugin-conf.d/

#-------------------------------------------------------------------------------
#   7. SCRIPTS
#-------------------------------------------------------------------------------

COPY entrypoint.sh .

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 8080

CMD [ "/bin/bash", "entrypoint.sh" ]

