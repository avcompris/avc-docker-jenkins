#/bin/bash

# File: avc-docker-jenkins/entrypoint.sh

set -e

#-------------------------------------------------------------------------------
#   1. VALIDATIONS
#-------------------------------------------------------------------------------

if [ -z "${JENKINS_USER}" ]; then
	echo "JENKINS_USER should be set" >&2
	echo "Exiting." >&2
	exit 1
elif [ -z "${JENKINS_PASSWORD}" ]; then
	echo "JENKINS_PASSWORD should be set" >&2
	echo "Exiting." >&2
	exit 1
fi

#-------------------------------------------------------------------------------
#   2. MUNIN NODE
#-------------------------------------------------------------------------------

MUNIN_NODE_NAME="${MUNIN_NODE_NAME:=jenkins}"
sed -i "s/^\s*#\?\s*host_name .*/host_name ${MUNIN_NODE_NAME}/g" /etc/munin/munin-node.conf

if [ -n "${MUNIN_NODE_ALLOW}" ]; then
	echo "allow ${MUNIN_NODE_ALLOW}" >> /etc/munin/munin-node.conf
fi

sed -i "s/JENKINS_USER/${JENKINS_USER}/g" /etc/munin/plugin-conf.d/jenkins_
sed -i "s/JENKINS_PASSWORD/${JENKINS_PASSWORD}/g" /etc/munin/plugin-conf.d/jenkins_

# start local munin-node
echo "Starting munin-node: ${MUNIN_NODE_NAME}..."
/etc/init.d/munin-node start

#-------------------------------------------------------------------------------
#   3. SSH CONFIG
#-------------------------------------------------------------------------------

mkdir -p /var/jenkins_home/.ssh

chown -R jenkins:jenkins /var/jenkins_home/.ssh

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

su - jenkins

# start jenkins
echo "Starting Jenkins..."
/sbin/tini -- /usr/local/bin/jenkins.sh

